package br.com.will.helloword;

import java.util.HashMap;
import java.util.Map;

public class Kata09 {
	
	public int calcularTotalcaixa(String produto){
		
		String [] produtos = produto.split(",");
		
		int a = 0;
		int b = 0;
		
		int valorA = 50;
		int valorB = 30;
		int valorC =0;
		int valorD =0;
		int qtdB = 0;
		Map<String, Integer> mapas = new HashMap<>();
		//For each do A
		for (String produtoA : produtos) {			
			if (produtoA.equalsIgnoreCase("A")) {
				mapas.put("A", valorA);
				valorA=valorA+50;				
				if(mapas.get("A") % 3 ==0){
					a++;
				}	
			}
					
		}
		
		//For each do B
		for (String produtoB : produtos) {			
			if (produtoB.equalsIgnoreCase("B")) {
				mapas.put("B", valorB);
				valorB=valorB+30;	
				qtdB = qtdB + 1;
				if(qtdB % 2 ==0){
					b++;
				}	
			}
			
		}
			
			//For each do C
			for (String produtoC : produtos) {			
				if (produtoC.equalsIgnoreCase("C")) {			
					
					valorC = valorC + 20;
									
				}
        }
			
			//For each do D
			for (String produtoD : produtos) {			
				if (produtoD.equalsIgnoreCase("D")) {			
					
					valorD = valorD + 15;
									
				}
				
					
		}
		
		
		int valorDescontoA = a*20;
		int valorTotalA = (mapas.get("A") == null ) ? 0 :  mapas.get("A");
		int totalA =  valorTotalA - valorDescontoA;
		
		int valorDescontoB = b*15;
		int valorTotalB = (mapas.get("B") == null ) ? 0 :  mapas.get("B");
		int totalB =  valorTotalB - valorDescontoB;
		
		return totalA + totalB + valorC + valorD;	
		
		
}}