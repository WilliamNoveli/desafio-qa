package br.com.will.helloword;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class Kata09Test {
	
	@Test
	public void testCaluculo(){
		Kata09 c = new Kata09();
	     
		assertEquals(0,c.calcularTotalcaixa(""));
		assertEquals(50,c.calcularTotalcaixa("A"));
		assertEquals(80,c.calcularTotalcaixa("A,B"));
		assertEquals(115,c.calcularTotalcaixa("C,D,B,A"));
		assertEquals(100,c.calcularTotalcaixa("A,A"));
		assertEquals(130,c.calcularTotalcaixa("A,A,A"));
		assertEquals(180,c.calcularTotalcaixa("A,A,A,A"));
		assertEquals(230,c.calcularTotalcaixa("A,A,A,A,A"));
		assertEquals(260,c.calcularTotalcaixa("A,A,A,A,A,A"));
		assertEquals(160,c.calcularTotalcaixa("A,A,A,B"));
		assertEquals(175,c.calcularTotalcaixa("A,A,A,B,B"));
		assertEquals(190,c.calcularTotalcaixa("A,A,A,B,B,D"));
		assertEquals(190,c.calcularTotalcaixa("D,A,B,A,B,A"));
		

	}

}
