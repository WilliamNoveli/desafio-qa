# encoding: UTF-8
# language: pt
@WhatsAppAlterarPerfilTeste
Funcionalidade: 
  Como usu�rio do WhatsApp Web, quero alterar as caracter�sticas do meu perfil

  Cen�rio: Verificar a altera��o do nome no perfil
   
  Dado que estou logado na pagina principal do WhatsAppWeb,
  E fiz a verifica�ao do QR code pelo celular, 
  Quando clico na foto do perfil na parte superior do lado esquerdo da tela, 
  E clico em editar em frente ao nome do usu�rio,
  E digito um novo nome,
  E clico para salvar em frente ao novo nome do usu�rio 
  Ent�o o nome � alterado no perfil com sucesso

  Cen�rio: Verificar a altera��o do recado no perfil
   
  Dado que estou logado na pagina principal do WhatsAppWeb,
  E fiz a verifica�ao do QR code pelo celular, 
  Quando clico na foto do perfil na parte superior do lado esquerdo da tela, 
  E clico em editar em frente ao recado do usu�rio,
  E digito um novo recado,
  E clico para salvar em frente ao novo recado do usu�rio 
  Ent�o o recado � alterado no perfil com sucesso
