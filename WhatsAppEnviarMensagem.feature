# encoding: UTF-8
# language: pt
@WhatsAppEnviarMensagemTeste
Funcionalidade:
  Como usu�rio do WhatsApp Web, quero enviar uma mensagem para meu contato da lista.
   
   
  Cen�rio: Verificar o envio da mensagem para meu primeiro contato da lista
   
  Dado que estou logado na pagina principal do WhatsAppWeb,
  E fiz a verifica�ao do QR code pelo celular, 
  Quando clico no primeiro contato da lista, 
  E digito a mensagem,
  E clico na seta para enviar, 
  Ent�o a mensagem ser� enviada com sucesso
  
  Cen�rio: Verificar o envio da mensagem pesquisando um contato da lista
  
  Dado que estou logado na pagina principal do WhatsAppWeb,
  E fiz a verifica�ao do QR code pelo celular,
  Quando clico na lupa para pesquisar o contato da lista
  E digito o nome de um contato da lista
  E clico em cima do nome do contato pesquisado
  E digito a mensagem,
  E clico na seta para enviar, 
  Ent�o a mensagem ser� enviada com sucesso
